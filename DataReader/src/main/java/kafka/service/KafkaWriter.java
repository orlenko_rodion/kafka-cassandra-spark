package kafka.service;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.kafka.client.producer.KafkaProducer;
import io.vertx.kafka.client.producer.KafkaProducerRecord;
import io.vertx.kafka.client.producer.RecordMetadata;

import java.util.HashMap;
import java.util.Map;

public class KafkaWriter {

    private static KafkaProducer<String, String> producer;

    private KafkaProducer<String, String> getProducer() {
        if (producer == null) {
            producer = createProducer();
        }
        return producer;
    }

    private KafkaProducer<String, String> createProducer() {
        Map<String, String> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        config.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        config.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        return io.vertx.kafka.client.producer.KafkaProducer.create(Vertx.vertx(), config);
    }

    public Future<RecordMetadata> write(String topic,String value) {
        Future<RecordMetadata> resultFuture = Future.future();
        KafkaProducerRecord<String, String> record = KafkaProducerRecord.create(topic, value);
        getProducer().write(record, resultFuture);
        return resultFuture;
    }
}
