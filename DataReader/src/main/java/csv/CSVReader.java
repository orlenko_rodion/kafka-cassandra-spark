package csv;



import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class CSVReader {

    private final Vertx vertx;

    public CSVReader(Vertx vertx) {
        this.vertx = vertx;
    }

    public Future<Void> readDataFromFile(String csvFile, String separator, Handler<List<String>> rowHandler) {
        Future<Void> resultFuture = Future.future();
        vertx.executeBlocking(blockingFuture -> {
            try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
                String row;
                while ((row = br.readLine()) != null) {
                    String[] spittedRow = row.split(separator);
                    rowHandler.handle(Arrays.asList(spittedRow));
                }
                blockingFuture.complete();
            } catch (IOException e) {
                e.printStackTrace();
                blockingFuture.fail(e);
            }
        }, false, resultFuture);

        return resultFuture;
    }
}