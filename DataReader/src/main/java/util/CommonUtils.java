package util;

import com.google.common.collect.ImmutableMap;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommonUtils {
    private static final Map<Integer, String> indexTofieldNameMap = new ImmutableMap.Builder<Integer, String>()
            .put(0, "sourceIP")
            .put(1, "destURL")
            .put(2, "visitDate")
            .put(3, "adRevenue")
            .put(4, "userAgent")
            .put(5, "countryCode")
            .put(6, "languageCode")
            .put(7, "searchWord")
            .put(8, "duration")
            .build();

    public static String getFieldNameByIndex(int index) {
        return indexTofieldNameMap.get(index);
    }

    public static List<String> getFilesInFolder(String folderPath) {
        File folder = new File(folderPath);
        return Arrays
                .stream(folder.listFiles())
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
    }
}
