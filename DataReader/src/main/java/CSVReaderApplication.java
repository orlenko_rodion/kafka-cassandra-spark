import csv.CSVReader;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import kafka.service.KafkaWriter;

import java.util.ArrayList;
import java.util.List;

import static util.CommonUtils.getFieldNameByIndex;
import static util.CommonUtils.getFilesInFolder;


public class CSVReaderApplication {

    private final static String DIRECTORY_PATH = "/home/user13/IdeaProjects/KafkaHelloWorld/DataReader/src/main/resources/uservisits";
    private final static String TOPIC_NAME = "user-visit";

    private final static KafkaWriter kafkaWriter = new KafkaWriter();
    private final static CSVReader csvReader = new CSVReader(Vertx.vertx());

    public static void main(String[] args) {
        List<Future> futureList = new ArrayList<>();

        try {

            for (String filePath : getFilesInFolder(DIRECTORY_PATH)) {

                futureList.add(csvReader.readDataFromFile(filePath, ",", splintedRow -> {
                    JsonObject jsonObject = mapToJson(splintedRow);
                    kafkaWriter
                            .write(TOPIC_NAME, jsonObject.toString())
                            .setHandler(recordMetadataAsyncResult -> {
                                if (recordMetadataAsyncResult.failed()) {
                                    System.out.println("Failed to write! cause = " + recordMetadataAsyncResult.cause());
                                }
                            });

                }));
                Thread.sleep(5000);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        CompositeFuture.all(futureList)
                .setHandler(compositeFutureAsyncResult -> {
                    if (compositeFutureAsyncResult.failed()) {
                        compositeFutureAsyncResult.cause().printStackTrace();
                    }
                    System.out.println("DONE!");
                });
    }

    private static JsonObject mapToJson(List<String> splintedRow) {
        JsonObject jsonObject = new JsonObject();
        for (int i = 0; i < splintedRow.size(); i++) {
            jsonObject.put(getFieldNameByIndex(i), splintedRow.get(i));
        }
        return jsonObject;
    }
}
