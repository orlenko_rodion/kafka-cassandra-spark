package kafka.service;

import io.vertx.core.Vertx;
import io.vertx.kafka.client.consumer.KafkaConsumer;

import java.util.HashMap;
import java.util.Map;

public class KafkaReader {

    private static KafkaConsumer<String, String> consumer;

    public KafkaConsumer<String, String> getConsumer(String consumerGroup) {
        if (consumer == null) {
            consumer = createConsumer(consumerGroup);
        }
        return consumer;
    }

    private static KafkaConsumer<String, String> createConsumer(String consumerGroup) {
        Map<String, String> config = new HashMap<>();
        config.put("bootstrap.servers", "localhost:9092");
        config.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        config.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        config.put("group.id", consumerGroup);
        config.put("zookeeper.session.timeout.ms", "400");
        config.put("zookeeper.sync.time.ms", "200");
        config.put("auto.commit.interval.ms", "1000");

        return KafkaConsumer.create(Vertx.vertx(), config);
    }
}
