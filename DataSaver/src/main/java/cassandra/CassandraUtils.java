package cassandra;


import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.ListenableFuture;
import io.vertx.core.Future;
import lombok.experimental.UtilityClass;

import static com.google.common.util.concurrent.Futures.*;

@UtilityClass
public class CassandraUtils {

    public static <T> Future<T> toVertxFuture(ListenableFuture<T> listenableFuture) {
        Future<T> resultVertxFuture = Future.future();

        addCallback(listenableFuture, new FutureCallback<T>() {
            public void onSuccess(T version) {
                resultVertxFuture.complete();
            }
            public void onFailure(Throwable t) {
                resultVertxFuture.fail(t);
            }
        });
        return resultVertxFuture;
    }
}
