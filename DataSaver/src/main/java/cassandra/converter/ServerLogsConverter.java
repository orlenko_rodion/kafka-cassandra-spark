package cassandra.converter;

import cassandra.dao.impl.entity.ServerLogsEntity;
import cassandra.dto.ServerLogsDto;

public class ServerLogsConverter {

    public ServerLogsEntity toEntity(ServerLogsDto dto) {
        ServerLogsEntity entity = new ServerLogsEntity();
        entity.setAdRevenue(dto.getAdRevenue());
        entity.setCountryCode(dto.getCountryCode());
        entity.setDestUrl(dto.getDestUrl());
        entity.setDuration(dto.getDuration());
        entity.setLanguageCode(dto.getLanguageCode());
        entity.setSearchWord(dto.getSearchWord());
        entity.setSourceIp(dto.getSourceIp());
        entity.setUserAgent(dto.getUserAgent());
        entity.setVisitDate(dto.getVisitDate().getTime());
        return entity;
    }
}
