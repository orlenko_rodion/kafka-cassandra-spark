package cassandra.dao.impl.entity;

import com.datastax.driver.core.LocalDate;
import com.datastax.driver.mapping.annotations.ClusteringColumn;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;

@Data
@Table(name = "server_logs")
public class ServerLogsEntity {

    @PartitionKey
    @Column(name = "country_code")
    private String countryCode;

    @ClusteringColumn
    @Column(name = "user_agent")
    private String userAgent;

    @ClusteringColumn(1)
    @Column(name = "visit_date")
    private LocalDate visitDate;

    @Column(name = "dest_url")
    private String destUrl;

    @Column(name = "source_ip")
    private String sourceIp;

    @Column(name = "ad_revenue")
    private float adRevenue;

    @Column(name = "language_code")
    private String languageCode;

    @Column(name = "search_word")
    private String searchWord;

    @Column(name = "duration")
    private int duration;


    public void setVisitDate(long visitTime) {
        this.visitDate = LocalDate.fromMillisSinceEpoch(visitTime);
    }

}