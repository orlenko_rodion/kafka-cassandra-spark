package cassandra.dao.impl;

import cassandra.dao.BaseDao;
import cassandra.dao.impl.entity.ServerLogsEntity;
import com.datastax.driver.mapping.annotations.Accessor;
import io.vertx.core.Future;

import static cassandra.CassandraUtils.toVertxFuture;

public class VisitStatDao extends BaseDao<ServerLogsEntity> {

    private final VisitStatAccessor accessor;

    public VisitStatDao() {
        super(ServerLogsEntity.class);
        accessor = mappingManager.createAccessor(VisitStatAccessor.class);
    }

    public Future<Void> saveAsync(ServerLogsEntity entity) {
        return toVertxFuture(mapper.saveAsync(entity));
    }

    @Accessor
    private interface VisitStatAccessor {
        // TODO: write queries if needed
    }
}
