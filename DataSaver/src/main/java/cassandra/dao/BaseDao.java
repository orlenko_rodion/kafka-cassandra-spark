package cassandra.dao;

import cassandra.connector.CassandraConnector;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

public abstract class BaseDao <T> {

    public static final String NODE = "127.0.0.1";
    public static final int PORT = 9042;
    public static final String KEYSPACE = "sandbox";

    protected final Session session;
    protected final MappingManager mappingManager;
    protected final Mapper<T> mapper;

    public BaseDao(Class<T> clazz) {
        CassandraConnector connector = new CassandraConnector();
        connector.connect(KEYSPACE,NODE, PORT);
        session = connector.getSession();
        mappingManager = new MappingManager(session);
        mapper = mappingManager.mapper(clazz);
    }
}
