package cassandra.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ServerLogsDto {

    @JsonProperty(value = "sourceIP")
    private String sourceIp;

    @JsonProperty(value = "destURL")
    private String destUrl;

    @JsonProperty(value = "visitDate")
    private Date visitDate;

    @JsonProperty(value = "adRevenue")
    private float adRevenue;

    @JsonProperty(value = "userAgent")
    private String userAgent;

    @JsonProperty(value = "countryCode")
    private String countryCode;

    @JsonProperty(value = "languageCode")
    private String languageCode;

    @JsonProperty(value = "searchWord")
    private String searchWord;

    @JsonProperty(value = "duration")
    private int duration;
}