import cassandra.converter.ServerLogsConverter;
import cassandra.dao.impl.VisitStatDao;
import cassandra.dto.ServerLogsDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import kafka.service.KafkaReader;

import java.io.IOException;

public class DataSaverApplication {
    private final static String TOPIC_NAME = "user-visit";
    public static final String CONSUMER_GROUP = "data_saver_group";

    private static final KafkaReader kafkaConsumer = new KafkaReader();
    private static final ServerLogsConverter converter = new ServerLogsConverter();
    private static final VisitStatDao statDao = new VisitStatDao();

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) {
        KafkaConsumer<String, String> consumer = kafkaConsumer.getConsumer(CONSUMER_GROUP);

        consumer.handler(record -> {
            long offset = record.offset();
            String value = record.value();
            ServerLogsDto serverLogsDto = mapJsonToDto(value, ServerLogsDto.class);
            statDao.saveAsync(converter.toEntity(serverLogsDto)).setHandler(voidAsyncResult -> {
                if (voidAsyncResult.failed()) {
                    voidAsyncResult.cause().printStackTrace();
                    return;
                }
                System.out.println(serverLogsDto);
                System.out.println(offset);
            });
        });

        consumer.subscribe(TOPIC_NAME);

    }

    private static <T> T mapJsonToDto(String jsonString, Class<T> clazz) {
        try {
            return objectMapper.readValue(jsonString, clazz);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
