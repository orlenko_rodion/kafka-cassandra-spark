
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import io.vertx.core.json.JsonObject;
import kafka.serializer.StringDecoder;
import org.apache.spark.api.java.JavaPairRDD;
import scala.Tuple2;

import org.apache.kafka.clients.consumer.ConsumerConfig;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.api.java.*;
import org.apache.spark.streaming.Durations;

import org.apache.spark.streaming.kafka.KafkaUtils;

public class VisitRateApplication {

    public static final String KAFKA_BROKERS = "localhost:9092";
    public static final String KAFKA_GROUP = "spark-group";
    public static final String KAFKA_TOPIC = "user-visit";
    public static final int JOB_PERIOD_SECONDS = 10;

    private static Map<String, Integer> resultMap = new ConcurrentHashMap<>();

    public static void main(String[] args) throws InterruptedException {
        SparkConf sparkConf = new SparkConf().setMaster("local").setAppName("JavaDirectKafkaWordCount");
        JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(JOB_PERIOD_SECONDS));

        JavaPairInputDStream<String, String> dStream = getStream(jssc);

        dStream.mapToPair(record -> {
                    JsonObject jsonObject = new JsonObject(record._2);
                    String countryCode = jsonObject.getString("countryCode");
                    return new Tuple2<>(countryCode, 1);
                })
                .reduceByKey((v1, v2) -> v1 + v2)
                .mapToPair(Tuple2::swap)
                .transformToPair(rddPair -> rddPair.sortByKey(false))
                .foreachRDD(VisitRateApplication::updateResultMapByRdd);
//                .print(10);

        jssc.start();
        jssc.awaitTermination();
    }

    private static void updateResultMapByRdd(JavaPairRDD<Integer, String> rdd) {
        rdd.collectAsMap().forEach((count, countryCode) -> {
            Integer insertedValue = resultMap.computeIfPresent(countryCode, (string, integer) -> integer + count);
            if (insertedValue == null) {
                resultMap.put(countryCode, count);
            }
        });
        System.out.println(resultMap);
    }


    private static JavaPairInputDStream<String, String> getStream(JavaStreamingContext jssc) {
        Map<String, String> kafkaParams = new HashMap<>();
        kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_BROKERS);
        kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG, KAFKA_GROUP);

        return KafkaUtils.createDirectStream(
                jssc,
                String.class,
                String.class,
                StringDecoder.class,
                StringDecoder.class,
                kafkaParams,
                Collections.singleton(KAFKA_TOPIC));
    }
}

